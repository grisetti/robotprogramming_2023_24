#include <iostream>

#include "grid_map.h"
// #include "isometry_2.h"
#include "laser_scan.h"
#include "laser_scanner.h"
#include "world_item.h"


using namespace std;

int main(int argc, char** argv) {
  if (argc < 2) {
    cout << "usage: " << argv[0] << " <image_file> <resolution>" << endl;
    return -1;
  }
  const char* filename = argv[1];
  float resolution = atof(argv[2]);

  cout << "Running " << argv[0] << " with arguments" << endl
       << "-filename:" << argv[1] << endl
       << "-resolution: " << argv[2] << endl;

  GridMap grid_map(0, 0, 0.1);
  grid_map.loadFromImage(filename, resolution);
  Canvas canvas;
  Eigen::Vector2f center = grid_map.grid2world(grid_map.origin());
  cerr << "center: " << center.transpose() << endl;
  cerr << "origin: " << grid_map.origin().transpose() << endl;

  grid_map.draw(canvas);

  WorldItem* items[100];
  memset(items, 0, sizeof(WorldItem*) * 100);

  World world_object(grid_map);
  items[0] = &world_object;

  Eigen::Isometry2f robot_in_world = Eigen::Isometry2f::Identity();
  robot_in_world.translation() << 5,-1;
  UnicyclePlatform robot(world_object, robot_in_world);
  robot.radius = 1;
  robot.tvel = 0;
  robot.rvel = 0;
  items[1] = &robot;

  LaserScan scan;
  Eigen::Isometry2f scanner_in_robot = Eigen::Isometry2f::Identity();
  scanner_in_robot.translation().x() = 1;
  LaserScanner scanner(scan, robot, scanner_in_robot, 10);
  scanner.radius = 0.5;
  items[2] = &scanner;

  while (true) {
    grid_map.draw(canvas);
    world_object.tick(0.01);
    world_object.draw(canvas, false);

    int ret = showCanvas(canvas, 0);
    std::cerr << "Key pressed: " << ret << std::endl;

    switch (ret) {
      case 81:  // left;
        robot.rvel += 0.1;
        break;
      case 82:  // up;
        robot.tvel += 0.1;
        break;
      case 83:  // right;
        robot.rvel -= 0.1;
        break;
      case 84:  // down;
        robot.tvel -= 0.1;
        break;
      case 32:  // space;
        robot.tvel = 0;
        robot.rvel = 0;
      default:;
    }
  }
}
