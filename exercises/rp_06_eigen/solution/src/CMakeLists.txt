add_executable(eigen_in_class
  eigen_in_class.cpp)

add_executable(grid_main
  grid_main.cpp
  grid_map.cpp
  draw_helpers.cpp
  laser_scan.cpp
  world_item.cpp
  laser_scanner.cpp)

target_link_libraries(grid_main
  ${OpenCV_LIBS}
  )

add_executable(test_grid_map
  test_grid_map.cpp
  grid_map.cpp
  draw_helpers.cpp)

target_link_libraries(test_grid_map
  ${OpenCV_LIBS}
  )

add_executable(simulator
  simulator.cpp
  grid_map.cpp
  draw_helpers.cpp
  laser_scan.cpp
  world_item.cpp
  laser_scanner.cpp
  )

target_link_libraries(simulator
  ${OpenCV_LIBS}
  )