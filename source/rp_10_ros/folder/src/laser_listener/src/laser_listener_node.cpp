#include <ros/ros.h>
#include <iostream>
#include <sensor_msgs/LaserScan.h>

using namespace std;

void laserCallback(const sensor_msgs::LaserScan& scan) {
  cerr << "print something" << endl;
}

int main(int argc, char** argv) {
  ros:: init(argc, argv, "boh");
  ros::NodeHandle n;
  string topic_name=argv[1]; 
  ros::Subscriber sub = n.subscribe<const sensor_msgs::LaserScan&>(topic_name, 10, laserCallback);
  ros::spin();
}
