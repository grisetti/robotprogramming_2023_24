#include <ros/ros.h>
#include <iostream>
#include <sensor_msgs/LaserScan.h>
#include <thread>

using namespace std;

int main(int argc, char** argv) {
  ros:: init(argc, argv, "bah");
  ros::NodeHandle n;
  string topic_name=argv[1]; 
  ros::Publisher pub=n.advertise<sensor_msgs::LaserScan>(topic_name, 10);
  sensor_msgs::LaserScan current_scan;
  while(1) {
    usleep(10000);
    pub.publish(current_scan);
    ros::spinOnce();
  }
}
