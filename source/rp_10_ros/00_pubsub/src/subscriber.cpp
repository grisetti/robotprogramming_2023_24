#include <ros/ros.h>
#include <std_msgs/String.h>

void onStringReceive(const std_msgs::StringConstPtr& message_in) {
  ROS_INFO("Received string [%s]", message_in->data.c_str());
}

int main(int argc, char** argv) {
  ros::init(argc, argv, "simple_subscriber");

  ros::NodeHandle nh;

  ros::Subscriber sub_handle =
      nh.subscribe<std_msgs::String>("/string_in", 10, onStringReceive);

  ros::spin();

  return 0;
}